﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class Player_NetworkSetup : NetworkBehaviour {

	// Use this for initialization
	void Start () {
	
        if(isLocalPlayer)
        {
            GetComponent<PlayerController>().enabled = true;
            GetComponent<Player>().enabled = true;

            Camera.main.GetComponent<CameraManager>().target = transform;
            Camera.main.GetComponent<CameraManager>().lookAhead = GetComponent<Player>().lookAhead;

        }

	}

}
