﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

[RequireComponent (typeof (PlayerController))]
[RequireComponent (typeof (RangedCombatManager))]
public class Player : LivingEntity {
    public Transform lookAhead;
	public float moveSpeed = 5;
    Vector3 target;

    Camera viewCamera;
	PlayerController controller;
    CombatManager combatManager;
    [HideInInspector]
    public Animator anim;

    // INPUT CONTROLS
    float h;
    float v;

    public bool isIdle = true;

    public EventSystem eventSystem;

    protected override void Start () {
        base.Start();
		controller = GetComponent<PlayerController> ();
		viewCamera = Camera.main;
        anim = GetComponent<Animator>();
        combatManager = GetComponent<CombatManager>();
    }

    void Update() {
        if (!dead) { 
            // Pick up command
            if (Input.GetKeyDown(KeyCode.Alpha1)) {
                if (combatManager.PickUpWeapon()) {
                    Debug.Log("Picked up weapon!");
                } else {
                    Debug.Log("No weapon nearby!");
                }
            }
        }
    }

	void FixedUpdate () {
        if (!dead) {
            //Movement Input
            //======================
            h = Input.GetAxisRaw("Horizontal");
            v = Input.GetAxisRaw("Vertical");

            Vector3 moveInput = new Vector3(h, 0, v);
            Vector3 moveVelocity = moveInput.normalized * moveSpeed;

            if (Mathf.Abs(moveVelocity.magnitude) < .25f) {
                anim.SetBool("isMoving", false);
            } else {
                anim.SetBool("isMoving", true);
            }

            // This stops the player from moving while firing
            if (combatManager.isFiring)
                moveVelocity = new Vector3();

            rb.velocity = moveVelocity;

            //Look Input
            //======================
            Ray ray = viewCamera.ScreenPointToRay(Input.mousePosition);
            Plane groundPlane = new Plane(Vector3.up, Vector3.zero);
            float rayDistance;

            if (groundPlane.Raycast(ray, out rayDistance)) {
                target = ray.GetPoint(rayDistance);

                // Height control the target
                target = new Vector3(target.x, controller.transform.position.y, target.z);
                controller.LookAt(target);// Get the dir normalized by the camera

                Vector3 moveDir = transform.forward;
                moveDir = (moveDir * moveVelocity.magnitude).normalized;

                // Increase inputs, using dot product to determine movement in a mouse dir
                anim.SetFloat("Forward", Vector3.Dot(moveInput, rb.transform.forward)*5);
                anim.SetFloat("Strafe", Vector3.Dot(moveInput, rb.transform.right)*5);
            }
        }
    }

    /// <summary>
    /// Check if the offhand could equip then pass this info
    /// to the mainhand slot
    /// </summary>
    void PickUpNearbyWeapon() {
        combatManager.PickUpWeapon();
    }

    void OnHitObject(Collider c, Vector3 hitPoint)
    {
        IDamageable damageableObject = c.GetComponent<IDamageable>();

        if (damageableObject != null)
        {
            damageableObject.TakeHit(1, hitPoint, transform.forward);
        }
    }

    public void RegisterHit(Collider c, Vector3 hitPoint) {
        OnHitObject(c, hitPoint);
    }

    public override void TakeDamage(float damage) {
        health -= damage;

        if (health <= 0 && !dead) {
            Die();
        } else {
            StartCoroutine(HurtAnim());
        }
    }

    public override void Die() {
        dead = true;
        StartCoroutine(DeathAnim());
    }

    IEnumerator DeathAnim() {
        anim.SetTrigger("Die");
        yield return new WaitForSeconds(5.0f);
        GameObject.Destroy(gameObject);
    }

    IEnumerator HurtAnim() {
        anim.SetTrigger("Hurt");
        yield return null;
    }
}
