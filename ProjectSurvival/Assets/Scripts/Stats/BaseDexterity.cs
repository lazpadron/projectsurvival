﻿[System.Serializable]
public class BaseDexterity : BaseStat
{

    public BaseDexterity()
    {
        StatName = "Dexterity";
        StatDescription = "Directly modifies a players stamina";
        StatType = StatTypes.DEXTERITY;
        StatBaseValue = 0;
        StatModifiedValue = 0;
    }

}