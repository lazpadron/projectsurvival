﻿[System.Serializable]
public class BaseStrength : BaseStat {

    public BaseStrength()
    {
        StatName = "Strength";
        StatDescription = "Directly modifies a players Health";
        StatType = StatTypes.STRENGTH;
        StatBaseValue = 0;
        StatModifiedValue = 0;
    }
	
}
