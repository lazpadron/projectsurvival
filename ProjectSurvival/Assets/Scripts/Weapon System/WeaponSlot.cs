﻿using UnityEngine;
using System.Collections;

public class WeaponSlot : MonoBehaviour {
    [HideInInspector]
    public Transform _transform;

    public Weapon myWeapon;
    public bool mainHand = true;
    bool equipped = false;

    void Start() {
        _transform = transform;
    }

    public void EquipWeapon(Weapon weapon) {
        SetAllCollidersStatus(weapon.gameObject, false);
        myWeapon = weapon;
        myWeapon.transform.position = _transform.position;
        myWeapon.transform.rotation = _transform.rotation;
        myWeapon.transform.parent = _transform;
        equipped = true;
    }

    public void Drop(Weapon weapon) {
        transform.parent = null;
        weapon.rb.isKinematic = false;
        SetAllCollidersStatus(weapon.gameObject, true);
        equipped = false;
    }
    
    public void SetAllCollidersStatus(GameObject weapon, bool active) {
        weapon.GetComponent<Rigidbody>().isKinematic = !active;
        foreach (Collider c in weapon.GetComponents<Collider>()) {
            c.enabled = active;
        }
    }
}
