﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RangedWeapon : Weapon {
	public Transform muzzle;
	public Projectile projectile;
	public float muzzleVelocity = 40;

    public float accuracy = 1f;
    float maxDegreesOff = 3f;

    public override void Attack(Vector3 direction, List<Enemy> nearEnemy) {
        Projectile newProjectile = Instantiate(projectile, muzzle.position, muzzle.rotation) as Projectile;

        newProjectile.transform.forward = direction;

        // Accuracy is handled here          
        float degreesOff = Random.Range(-maxDegreesOff, maxDegreesOff);
        newProjectile.transform.Rotate(0f, degreesOff, 0f);
        //DebugShot(newProjectile.transform);
        newProjectile.SetSpeed(muzzleVelocity);
    }

    void DebugShot(Transform projectile) {
        GameObject aimConeMin = new GameObject();
        aimConeMin.transform.position = projectile.position;
        aimConeMin.transform.rotation = projectile.rotation;

        aimConeMin.transform.Rotate(0f, -maxDegreesOff, 0f);
        Vector3 minDir = aimConeMin.transform.forward;

        GameObject aimConeMax = new GameObject();
        aimConeMax.transform.position = projectile.position;
        aimConeMax.transform.rotation = projectile.rotation;
        aimConeMax.transform.Rotate(0f, maxDegreesOff, 0f);

        Vector3 maxDir = aimConeMax.transform.forward;

        Debug.DrawRay(projectile.position, minDir * 20, Color.blue, 3f);
        Debug.DrawRay(projectile.position, maxDir * 20, Color.blue, 3f);
    }
}
