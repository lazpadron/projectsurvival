﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public enum WeaponType {
    MELEE,
    RANGED,
    SHIELD,
    MINING,
    HARVESTING,
    CRAFTING
}

public abstract class Weapon : MonoBehaviour {
    // Sample Melee Axe for tree cutting    
    public string _name;
    public string _description;
    public WeaponType[] _weaponType = { WeaponType.MELEE, WeaponType.HARVESTING };

    public int damage;
    public int currDurability;
    public int maxDurability;
    public float range;
    public Rigidbody rb;
    
    void Start() {
        rb = GetComponent<Rigidbody>();
        rb.isKinematic = false;
    }

    public abstract void Attack(Vector3 direction, List<Enemy> nearEnemy);

    public void AltAttack() {
        // Do an alt attack here
    }

    public bool CheckWeaponType(WeaponType wType) {
        foreach (WeaponType w in _weaponType) {
            if (w == wType)
                return true;
        }
        return false;
    }
}
