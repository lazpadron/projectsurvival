﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class HarvestingAxe : Weapon {
    public Enemy currEnemy;

    public override void Attack(Vector3 direction, List<Enemy> nearEnemy) {
        // Melee Attack here
        Vector3 pos = transform.position;
        for (int i = 0; i < nearEnemy.Count; i++) {
            Vector3 vec = nearEnemy[i].transform.position;
            Vector3 dirToEnemy = vec - pos;

            // Check for in front of the player
            Debug.Log("Dot Product: " + Vector3.Dot(dirToEnemy, direction));
            if (Vector3.Dot(dirToEnemy.normalized, direction.normalized) < 0.7) {
                currEnemy = nearEnemy[i];
                if (currEnemy.health <= damage) {
                    nearEnemy.RemoveAt(i);
                }
                currEnemy.TakeHit(damage, transform.position, dirToEnemy);
            }
        }
    }
}
