﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum CombatStance {
    MELEE, HEAVYMELEE, RANGED    
}

public class CombatManager : MonoBehaviour {
    Player myPlayer;
    Transform _transform;
    List<Enemy> nearbyEnemies = new List<Enemy>();

    public Weapon equippedWeapon;
    public float pickUpRadius = 5.0f;
    
    public WeaponSlot mainHandSlot;
    public WeaponSlot offHandSlot;

    public LayerMask enemyMask;
    public LayerMask weaponMask;

    [HideInInspector]
    public CombatStance combatStance;
    [HideInInspector]
    public bool isFiring = false;

    public void Start() {
        myPlayer = GetComponent<Player>();
        _transform = transform;
    }

    void Update() {
        LeftMouseClick();
    }

    public void Attack(Vector3 target) {
        if (equippedWeapon != null) {
            equippedWeapon.Attack(target, nearbyEnemies);
        }
    }

    public bool PickUpWeapon() {
        // INSERT CODE TO CHECK FOR ITEMS NEARBY
        float radius = .5f;
        Weapon weapon = null;
        
        while (radius <= pickUpRadius) {
            Collider[] weapons = Physics.OverlapSphere(_transform.position, radius, weaponMask);
            if (weapons.Length > 0) {
                List<Weapon> wepList = new List<Weapon>();
                foreach(Collider col in weapons) {
                    wepList.Add(col.gameObject.GetComponent<Weapon>());
                }
                int rand = Random.Range(0, wepList.Count);
                weapon = wepList[rand];
                break;
            }
            radius += .5f;
            Debug.Log("No match, trying a radius of " + radius);
        }

        //TODO: Add in the equip functionality
        if (weapon != null) {
            if (weapon.CheckWeaponType(WeaponType.SHIELD)) {
                Debug.Log("Equipped shield weapon!");
                offHandSlot.EquipWeapon(weapon);
            } else if (weapon.CheckWeaponType(WeaponType.RANGED)) {
                Debug.Log("Equipped ranged weapon!");
                offHandSlot.EquipWeapon(weapon);
            } else {
                Debug.Log("Equipped melee weapon!");
                //mainHandSlot.EquipWeapon(weapon);        
            }
            EquipWeapon(weapon);
            return true;
        } else {
            return false;
        }
    }

    void EquipWeapon(Weapon weapon) {
        equippedWeapon = weapon;
    }

    void LeftMouseClick() {
        if (Input.GetMouseButtonDown(0) && !isFiring) {
            RaycastHit hitInfo = new RaycastHit();
            bool hit = Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hitInfo);
            if (hit) {
                //inMeleeRange = false;
                bool inMeleeRange = Vector3.Distance(hitInfo.transform.position, transform.position) < equippedWeapon.range;

                if (hitInfo.transform.gameObject.tag == "Resource" && inMeleeRange) {
                    myPlayer.RegisterHit(hitInfo.collider, hitInfo.point);
                } else {
                    StartCoroutine(Attack());
                }
            } else {
                Debug.Log("No hit");
            }
        }
    }
    
    IEnumerator Attack() {
        isFiring = true;
        myPlayer.anim.SetTrigger("Shoot");
                
        float time = 0.6f;
        RuntimeAnimatorController ac = myPlayer.anim.runtimeAnimatorController;    //Get Animator controller
        for (int i = 0; i < ac.animationClips.Length; i++)                 //For all animations
        {
            if (ac.animationClips[i].name == "Shoot")        //If it has the same name as your clip
            {
                time = ac.animationClips[i].length;
            }
        }

        yield return new WaitForSeconds(time);
        equippedWeapon.Attack(transform.forward, nearbyEnemies);
        isFiring = false;
    }
}
