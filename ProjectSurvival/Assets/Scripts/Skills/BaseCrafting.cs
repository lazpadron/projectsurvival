﻿[System.Serializable]
public class BaseCrafting : BaseSkill
{

    public BaseCrafting()
    {
        SkillName = "Crafting";
        SkillDescription = "Building the essentials to push your team to the limits.";
        SkillType = SkillTypes.CRAFTING;
        SkillBaseValue = 0;
        SkillModifiedValue = 0;
    }

}
