﻿[System.Serializable]
public class BaseMelee : BaseSkill
{

    public BaseMelee()
    {
        SkillName = "Melee";
        SkillDescription = "Using any melee weapon in order to crush your enemies.";
        SkillType = SkillTypes.MELEE;
        SkillBaseValue = 0;
        SkillModifiedValue = 0;
    }

}

