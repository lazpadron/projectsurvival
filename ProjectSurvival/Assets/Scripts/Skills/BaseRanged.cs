﻿[System.Serializable]
public class BaseRanged : BaseSkill
{

    public BaseRanged()
    {
        SkillName = "Ranged";
        SkillDescription = "Using a bow to take out far away opponents.";
        SkillType = SkillTypes.RANGED;
        SkillBaseValue = 0;
        SkillModifiedValue = 0;
    }

}
