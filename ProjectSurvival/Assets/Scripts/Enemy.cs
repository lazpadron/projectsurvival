﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof (NavMeshAgent))]
public class Enemy : LivingEntity {
    public enum State
    {
        Idle, Wandering, Chasing, Attacking
    };

    public State currentState;

    public ParticleSystem deathEffect;

	NavMeshAgent pathfinder;
	Transform target;
    LivingEntity targetEntity;
    Material skinMaterial;

    Color originalColor;

    FieldOfView fov;
    public Vector3 wanderPoint;
    public Vector3 heardPoint;

    float attackDistanceThreshold = .5f;
    float timeBetweenAttacks = 1;
    float damage = 1;

    float nextAttackTime;
    float myCollisionRadius;
    float targetCollisionRadius;

    public bool hasTarget;
    public bool moving;
    public bool heardSomething;
    public bool destinationReached;
    public bool DEBUG_NOATTACK = false;

	protected override void Start () {
        base.Start();
		pathfinder = GetComponent<NavMeshAgent> ();
        skinMaterial = GetComponent<Renderer>().material;
        originalColor = skinMaterial.color;
        fov = GetComponent<FieldOfView>();

        currentState = State.Wandering;
        hasTarget = false;
        moving = true;
        heardSomething = false;

        destinationReached = false;
        wanderPoint = randomWanderingPoint();
 
        StartCoroutine(UpdatePath());
    }

    public override void TakeHit(float damage, Vector3 hitPoint, Vector3 hitDirection)
    {
        if (damage >= health)
        {
            Destroy(Instantiate(deathEffect.gameObject, hitPoint, Quaternion.FromToRotation(Vector3.forward, hitDirection)) as GameObject, deathEffect.startLifetime);
        }
        base.TakeHit(damage, hitPoint, hitDirection);
        StartCoroutine(DamageAnim());
    }

    void OnTargetDeath()
    {
        hasTarget = false;
        currentState = State.Idle;
    }

	void Update () {

        //we do not see the player
        if (fov.visibleTargets.Count == 0)
        {
            currentState = State.Wandering;

            if (hasTarget)
            {
                pathfinder.ResetPath();
                hasTarget = false;
            }

            if (fov.audibleTargets.Count > 0 && !heardSomething)
            {
                if (fov.audibleTargets[0] != null)
                {

                    target = fov.audibleTargets[0].transform;

                    heardPoint = target.position;
                    pathfinder.ResetPath();
                    heardSomething = true;

                }
            }

        }
        else if (fov.visibleTargets.Count > 0)
        {
            currentState = State.Chasing;
            hasTarget = true;

            //target may be destroyed during a call to this function, make sure it hasnt been destroyed
            if(fov.visibleTargets[0] != null) { 
                target = fov.visibleTargets[0].transform;
                targetEntity = target.GetComponent<LivingEntity>();
                targetEntity.OnDeath += OnTargetDeath;

                myCollisionRadius = GetComponent<CapsuleCollider>().radius;
                targetCollisionRadius = target.GetComponent<CapsuleCollider>().radius;
            }
        }

        if (hasTarget && !targetEntity.dead)
        {          
            if (Time.time > nextAttackTime)
            {
                float sqrDstToTarget = (target.position - transform.position).sqrMagnitude;
                if (sqrDstToTarget < Mathf.Pow(attackDistanceThreshold + myCollisionRadius + targetCollisionRadius, 2))
                {
                    nextAttackTime = Time.time + timeBetweenAttacks;
                    if (!DEBUG_NOATTACK) {
                        StartCoroutine(Attack());
                    }                    
                }
            }
        } else {
            currentState = State.Wandering;
            hasTarget = false;
            //pathfinder.ResetPath();
        }
    }

    Vector3 randomWanderingPoint()
    {
        Vector3 randLocation = Random.insideUnitSphere * 20;

        randLocation += transform.position;

        NavMeshPath path = new NavMeshPath();
        bool canReach = NavMesh.CalculatePath(transform.position, randLocation, NavMesh.AllAreas, path);

        if (!canReach) //call the function again;
        {
            randomWanderingPoint();
        }

        randLocation.y = 1;

        return randLocation;
    }

    IEnumerator Attack()
    {
        currentState = State.Attacking;
        pathfinder.Stop();

        Vector3 originalPosition = transform.position;
        Vector3 dirToTarget = (target.position - transform.position).normalized;
        Vector3 attackPosition = target.position - dirToTarget * (myCollisionRadius);

        float attackSpeed = 3;
        float percent = 0;

        skinMaterial.color = Color.red;
        bool hasAppliedDamage = false;

        while(percent <= 1)
        {
            if(percent >= 0.5f && !hasAppliedDamage)
            {
                hasAppliedDamage = true;
                targetEntity.TakeDamage(damage);
            }

            percent += Time.deltaTime * attackSpeed;

            float interpolation = (-Mathf.Pow(percent, 2) * percent + percent) * 4;
            transform.position = Vector3.Lerp(originalPosition, attackPosition, interpolation);

            yield return null;
        }

        skinMaterial.color = originalColor;
        pathfinder.Resume();
        currentState = State.Chasing;
    }

    IEnumerator UpdatePath() {
		float refreshRate = 0.40f;

		while (moving) {

            if (!hasTarget) //i dont have a target
            {
                if (!heardSomething) // i havent heard anything
                {
                    if (destinationReached) { //i have reached my destination

                        destinationReached = false;

                        wanderPoint = randomWanderingPoint();
                    }
                }
                else
                {
                    wanderPoint = heardPoint;
                    if(destinationReached)
                    {
                        heardSomething = false;
                    }
                }

                Vector3 dirToTarget = (wanderPoint - transform.position).normalized;
                Vector3 targetPosition = wanderPoint - dirToTarget;
                if (!dead)
                {
                    pathfinder.SetDestination(targetPosition);
                }
            }
            else
            {
                destinationReached = false;
                heardSomething = false;
                Vector3 dirToTarget = (target.position - transform.position).normalized;
                Vector3 targetPosition = target.position - dirToTarget;
                if (!dead)
                {
                    pathfinder.SetDestination(targetPosition);
                }
            }


            // Check if we've reached the destination
            if (!pathfinder.pathPending)
            {
                if (pathfinder.remainingDistance <= pathfinder.stoppingDistance)
                {
                    if (!pathfinder.hasPath || pathfinder.velocity.sqrMagnitude == 0f)
                    {
                        destinationReached = true;
                    }
                }
            }

            yield return new WaitForSeconds(refreshRate);
        }
	}

    void OnDestroy() {
        StopAllCoroutines();
    }

    void ResetPath() {
        rb.velocity = new Vector3();
        StartCoroutine(UpdatePath());
    }

    IEnumerator DamageAnim() {
        bool kinematic = rb.isKinematic;
        //rb.isKinematic = false;
        WaitForSeconds frameDelay = new WaitForSeconds(.1f);
        foreach (Material mat in skinMaterials) {
            mat.color = Color.red;
        }
        yield return frameDelay;
        yield return frameDelay;
        foreach (Material mat in skinMaterials) {
            mat.color = Color.yellow;
        }
        yield return frameDelay;
        foreach (Material mat in skinMaterials) {
            mat.color = Color.white;
        }
        yield return null;
        // Return meshes to original colors
        for (int i = 0; i < skinMaterials.Count; i++) {
            skinMaterials[i].color = originalColors[i];
        }

        //rb.isKinematic = kinematic;
        ResetPath();
    }
}
