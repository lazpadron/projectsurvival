﻿using UnityEngine;
using System.Collections;

public class Menu : MonoBehaviour {

    public GameObject menu; // Assign in inspector
    private bool isShowing;

    void Start()
    {
        isShowing = false;
    }

    // Update is called once per frame
    void Update () {

        if (Input.GetKeyDown("tab"))
        {
            isShowing = !isShowing;
            menu.SetActive(isShowing);
        }
    }
}
