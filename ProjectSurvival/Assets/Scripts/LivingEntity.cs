﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LivingEntity : MonoBehaviour, IDamageable {
    public float startingHealth;
    public float health;
    
    [HideInInspector]
    public bool dead;
    [HideInInspector]
    public List<Material> skinMaterials;
    [HideInInspector]
    public List<Color> originalColors;
    [HideInInspector]
    public Rigidbody rb;

    public event System.Action OnDeath;

    protected virtual void Start()
    {
        health = startingHealth;
        skinMaterials = new List<Material>();
        rb = GetComponent<Rigidbody>();

        if (GetComponent<Renderer>() == null) {
            SkinnedMeshRenderer mesh = GetComponentInChildren<SkinnedMeshRenderer>();
            foreach (Material mat in mesh.materials) {
                skinMaterials.Add(mat);
                originalColors.Add(mat.color);
            }
        } else {
            skinMaterials.Add(GetComponent<Renderer>().material);
            originalColors.Add(skinMaterials[0].color);
        }
    }

	public virtual void TakeHit(float damage, Vector3 hitPoint, Vector3 hitDirection)
    {
        //Later, do some stuff here with hit variable
        rb.AddExplosionForce(10f, hitPoint, 5f, .15f, ForceMode.Impulse);
        TakeDamage(damage);
    }

    public virtual void TakeDamage(float damage)
    {
        health -= damage;

        if (health <= 0 && !dead)
        {
            Die();
        }
    }

    public virtual void Die() {
        dead = true;
        if (OnDeath != null) {
            OnDeath();
        }
        GameObject.Destroy(gameObject);
    }
}
