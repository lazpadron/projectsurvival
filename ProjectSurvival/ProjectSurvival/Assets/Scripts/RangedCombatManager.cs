﻿using UnityEngine;
using System.Collections;

public class RangedCombatManager : MonoBehaviour {

	public Transform weaponHold;
	public RangedWeapon baseRangeWeapon;
	RangedWeapon equippedGun;

	public void Start() {
		if (baseRangeWeapon != null) {
			EquipRangedWeapon (baseRangeWeapon);
		}
	}

	public void EquipRangedWeapon(RangedWeapon gunToEquip) {
		if (equippedGun != null) {
			Destroy (equippedGun.gameObject);
		}
		equippedGun = Instantiate (gunToEquip, weaponHold.position, weaponHold.rotation) as RangedWeapon;
		equippedGun.transform.parent = weaponHold;
	}

	public void Shoot(Vector3 target) {
		if (equippedGun != null) {
			equippedGun.Shoot (target);
		}
	}
}
