﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class Player_SyncRotation : NetworkBehaviour {

    [SyncVar]
    private Quaternion syncPlayerRotation;

    [SerializeField]
    private Transform playerTransform;

    [SerializeField]
    private float lerpRate = 15;

    private Quaternion lastPlayerRotation;
    private float threshold = 5;

	// Use this for initialization
	void Start () {
	
	}

    void Update()
    {
        LerpRotation();
    }

    void FixedUpdate () {
        TransmitRotation();
    }

    void LerpRotation()
    {
        if (!isLocalPlayer) { 
            playerTransform.rotation = Quaternion.Lerp(playerTransform.rotation, syncPlayerRotation, Time.deltaTime * lerpRate);
        }
    }

    [Command]
    void CmdProvideRotationToServer(Quaternion playerRotation)        
    {
        syncPlayerRotation = playerRotation;
    }

    [ClientCallback]
    void TransmitRotation()
    {
        if (isLocalPlayer && Quaternion.Angle(playerTransform.rotation, lastPlayerRotation) > threshold)
        {
            CmdProvideRotationToServer(playerTransform.rotation);
            lastPlayerRotation = playerTransform.rotation;
        }
    }
}
