﻿using UnityEngine;
using System.Collections;

public class CameraManager : MonoBehaviour {
    public Transform target;
    public Transform lookAhead;
    public float minDistance = 15f;
    public float smoothing;
    Vector3 offset = new Vector3(0, 30, -50);

    void Start() {
        Application.targetFrameRate = 60;
    }

    // Update is called once per frame
    void LateUpdate() {
        if (target != null) {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            Plane groundPlane = new Plane(Vector3.up, Vector3.zero);
            float rayDistance;
            Vector3 newPos;

            if (groundPlane.Raycast(ray, out rayDistance)) {
                newPos = ray.GetPoint(rayDistance);
                //Debug.Log("Distance from player to mouse position: " + Vector3.Distance(target.position, newPos));
                bool inRange = true;
                inRange = Vector3.Distance(target.position, newPos) < minDistance;

                if (inRange) {
                    transform.position = Vector3.Lerp(transform.position, target.position + offset, smoothing / 10f);
                } else {
                    transform.position = Vector3.Lerp(transform.position, lookAhead.position + offset, smoothing / 10f);
                }
            }
        }
    }
}
