﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

[RequireComponent (typeof (PlayerController))]
[RequireComponent (typeof (RangedCombatManager))]
public class Player : LivingEntity {
    public Transform lookAhead;
	public float moveSpeed = 5;
    public float meleeRange = 25f;
    bool isFiring = false;
    bool inMeleeRange = false;
    Vector3 target;

    Camera viewCamera;
	PlayerController controller;
	RangedCombatManager rangedManager;
    Animator anim;

    public EventSystem eventSystem;

    protected override void Start () {
        base.Start();
		controller = GetComponent<PlayerController> ();
        rangedManager = GetComponent<RangedCombatManager>();
		viewCamera = Camera.main;
        anim = GetComponent<Animator>();
	}

	void FixedUpdate () {
		//Movement Input
		Vector3 moveInput = new Vector3 (Input.GetAxisRaw ("Horizontal"), 0, Input.GetAxisRaw ("Vertical"));
		Vector3 moveVelocity = moveInput.normalized * moveSpeed;

        if (Mathf.Abs(moveVelocity.magnitude) < .5f) {
            anim.SetBool("isMoving", false);
        } else {
            anim.SetBool("isMoving", true);
        }

        if (!isFiring)
		    controller.Move (moveVelocity);

        //Look Input
        Ray ray = viewCamera.ScreenPointToRay (Input.mousePosition);
		Plane groundPlane = new Plane (Vector3.up, Vector3.zero);
		float rayDistance;

		if (groundPlane.Raycast (ray, out rayDistance)) {
			target = ray.GetPoint (rayDistance);

            Debug.DrawLine (new Vector3(target.x, target.y + 3f, target.z), target, Color.red);

            // Height control the target
            target = new Vector3(target.x, controller.transform.position.y, target.z);
            controller.LookAt(target);
		}

        //Weapon Input
        LeftMouseClick();
    }

    void LeftMouseClick()
    {
        if (Input.GetMouseButtonDown(0))
        {

            RaycastHit hitInfo = new RaycastHit();
            bool hit = Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hitInfo);
            if (hit)
            {
                //Debug.Log("Hit " + hitInfo.transform.gameObject.name);
                //inMeleeRange = false;
                inMeleeRange = Vector3.Distance(hitInfo.transform.position, transform.position) < meleeRange;

                if (hitInfo.transform.gameObject.tag == "Resource" && inMeleeRange)
                {
                    OnHitObject(hitInfo.collider, hitInfo.point);
                }
                else {
                    StartCoroutine(Shoot());
                }
            }
            else {
                Debug.Log("No hit");
            }
        }
    }

    void OnHitObject(Collider c, Vector3 hitPoint)
    {
        IDamageable damageableObject = c.GetComponent<IDamageable>();

        if (damageableObject != null)
        {
            damageableObject.TakeHit(1, hitPoint, transform.forward);
        }
    }

    IEnumerator Shoot() {
        isFiring = true;
        anim.SetTrigger("Shoot");
        yield return new WaitForSeconds(.5f);
        rangedManager.Shoot(target);
        isFiring = false;
    }

    public override void TakeDamage(float damage) {
        health -= damage;

        if (health <= 0 && !dead) {
            Die();
        } else {
            anim.SetTrigger("Hurt");
        }
    }

    public override void Die() {
        dead = true;
        StartCoroutine(DeathAnim());
    }

    IEnumerator DeathAnim() {
        anim.SetTrigger("Die");
        yield return new WaitForSeconds(5.0f);
        GameObject.Destroy(gameObject);
    }
}
