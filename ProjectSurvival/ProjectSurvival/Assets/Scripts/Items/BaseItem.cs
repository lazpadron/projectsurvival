﻿using UnityEngine;
using System.Collections.Generic;

public class BaseItem {

    private string _name;
    private string _description;
    private float _value;
    private ItemTypes _type;

    public BaseItem()
    {
        ItemName = "Item" + Random.Range(0, 101);
        ItemDescription = ItemName + " is an awesome item!";
        ItemValue = Random.Range(10, 500);
        ItemType = ItemTypes.EQUIPMENT;
    }

    public enum ItemTypes
    {
        EQUIPMENT,
        WEAPON
    }

    public string ItemName
    {
        get { return _name; }
        set { _name = value; }
    }

    public string ItemDescription
    {
        get { return _description; }
        set { _description = value; }
    }

    public float ItemValue
    {
        get { return _value; }
        set { _value = value; }
    }

    public ItemTypes ItemType
    {
        get { return _type; }
        set { _type = value; }
    }

}
