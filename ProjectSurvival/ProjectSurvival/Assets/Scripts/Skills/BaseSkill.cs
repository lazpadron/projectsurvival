﻿using System.Collections;

public class BaseSkill {

    private string _name;
    private string _description;
    private float _baseValue;
    private float _modifiedValue;
    private SkillTypes _type;

    public enum SkillTypes
    {
        MELEE,
        RANGED,
        CRAFTING,
        HARVESTING
    }

    public string SkillName
    {
        get { return _name; }
        set { _name = value; }
    }

    public string SkillDescription
    {
        get { return _description; }
        set { _description = value; }
    }

    public float SkillBaseValue
    {
        get { return _baseValue; }
        set { _baseValue = value; }
    }

    public float SkillModifiedValue
    {
        get { return _modifiedValue; }
        set { _modifiedValue = value; }
    }

    public SkillTypes SkillType
    {
        get { return _type; }
        set { _type = value; }
    }
}
