﻿[System.Serializable]
public class BaseHarvesting : BaseSkill
{

    public BaseHarvesting()
    {
        SkillName = "Harvesting";
        SkillDescription = "Chopping trees and gathring ores.";
        SkillType = SkillTypes.HARVESTING;
        SkillBaseValue = 0;
        SkillModifiedValue = 0;
    }

}
