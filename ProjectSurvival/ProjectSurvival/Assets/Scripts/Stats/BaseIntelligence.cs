﻿[System.Serializable]
public class BaseIntelligence : BaseStat
{

    public BaseIntelligence()
    {
        StatName = "Intelligence";
        StatDescription = "Directly modifies a players mana";
        StatType = StatTypes.INTELLIGENCE;
        StatBaseValue = 0;
        StatModifiedValue = 0;
    }

}