﻿using UnityEngine;
using System.Collections;

public class RangedWeapon : MonoBehaviour {
	public Transform muzzle;
	public Projectile projectile;
	public float msBetweenShots = 2000;
	public float muzzleVelocity = 40;

    public float accuracy = 1f;
    float maxDegreesOff = 3f;

	float nextShotTime;

    void Start() {
        nextShotTime = Time.time;
    }

    public void Shoot(Vector3 target) {

        //if (Time.time > nextShotTime) {
            nextShotTime = Time.time + msBetweenShots / 1000;
            Projectile newProjectile = Instantiate(projectile, muzzle.position, muzzle.rotation) as Projectile;

            newProjectile.transform.forward = target - newProjectile.transform.position;
            //DebugShot(newProjectile.transform);

            // Add accuracy code here            
            float degreesOff = Random.Range(-maxDegreesOff, maxDegreesOff);
            newProjectile.transform.Rotate(0f, degreesOff, 0f);
            //Debug.Log("Degrees off: " + degreesOff);

            //Debug.DrawRay(newProjectile.transform.position, newProjectile.transform.forward * 20, Color.green, 3f);

            newProjectile.SetSpeed(muzzleVelocity);
        //}
    }

    void DebugShot(Transform projectile) {
        GameObject aimConeMin = new GameObject();
        aimConeMin.transform.position = projectile.position;
        aimConeMin.transform.rotation = projectile.rotation;

        aimConeMin.transform.Rotate(0f, -maxDegreesOff, 0f);
        Vector3 minDir = aimConeMin.transform.forward;

        GameObject aimConeMax = new GameObject();
        aimConeMax.transform.position = projectile.position;
        aimConeMax.transform.rotation = projectile.rotation;
        aimConeMax.transform.Rotate(0f, maxDegreesOff, 0f);

        Vector3 maxDir = aimConeMax.transform.forward;

        Debug.DrawRay(projectile.position, minDir * 20, Color.blue, 3f);
        Debug.DrawRay(projectile.position, maxDir * 20, Color.blue, 3f);
    }
}
